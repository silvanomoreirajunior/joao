// Escreva uma função que dada uma string composta por substrings separadas por um
// dado caractere, retorne todas as suas substrings da string original. Por exemplo, dado o
// separador ‘/’, a string “/Fla/Flu/Bota/” será decomposta em:
// • Fla
// • Flu
// • Bota
// O protótipo da função é o seguinte:

#include<stdio.h> 
#include<stdlib.h>
#include<string.h> 

char ** stringSplit (const char *text, char separator, int length){
    char **strings; 
    int i, j, k = 0, count = 0, sepNum =  0; 
    int start=0, end=length; 
    
    for(i = 0; i<length; i++){
        if(text[i]==separator)
            sepNum++; 
    }

    // Alocando espaço para as strings. (Número de separadores menos 1)
    strings = malloc((sepNum -1) * sizeof(char *)); 
    
    // Passeando por toda extensão da string
    for(i=0; i<length; i++){
        //Quebrando a string pelos separadores
        if(text[0]!=separator){ 
            printf("Indique o separador como primeiro caractere \n"); 
            return strings; 
        }
        if(text[i]==separator && i > start){
            //alocando o tamanho da substring
            strings[count]=malloc(sizeof(char) * (i - start + 1)); 
            //Salvando substring.
            strings[count][i - start] = '\0'; 
            for(j=start+1; j<i; j++){
                strings[count][k] = text[j]; 
                k++; 
                
            }
            
            printf("Strings: %s \n", strings[count]); 
            // Atualizando quantidade de strings
            count++; 
            
            // Redefinindo marcador
            start = i; 
            k = 0;
        }
    }

    return strings; 
    
}

int main(int argc, char *argv[]){
    char *text, separator; 
    // char **strings; 
    int length, sepNum; 
    
    printf("Entre com o separador: \n"); 
    scanf("%c",&separator);

    printf("Entre com o texto: \n"); 
    scanf("%s",text); 

    length = strlen(text); 

    char **strings = stringSplit(text, separator, length); 

    free(strings); 
    return 0; 
}

